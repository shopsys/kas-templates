module.exports = function(grunt) {
    
    grunt.initConfig({
        
        pkg: grunt.file.readJSON('package.json'),
        
        less: {
            front: {
                files: {
                    'assets/css/style.css': 'assets/less/style.less'
                },
                options: {
                    compress: true
                }
            },
			print: {
				files: {
					'assets/css/print.css': 'assets/less/print.less'
				},
				options: {
					compress: true
				}
			}
        },
        
        postcss: {
            options: {
                processors: [
                    require('autoprefixer')({browsers: ['last 5 versions', 'ie 8', 'ie 9']})
                ]
            },
            dist: {
                src: 'assets/css/*.css'
            }
        },
        
        watch: {
            styles: {
                files: ['assets/less/*.less', 'assets/less/components/*.less'],
                tasks: ['less', 'postcss']
            },
            livereload: {
                options: {
                    livereload: true
                },
                files: ['assets/css/*.css']
            }
        }
    });
    
    
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-postcss');
    
    
    grunt.registerTask('default', ['less', 'postcss']);
    
};
