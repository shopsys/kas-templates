$( document ).ready(function() {

    //Selectric
    if($('select.selectric').size()) {
    
        $('select.selectric').selectric();
    
    }
    //Selectric
    
    
    
    //Product list filter - price slider
    if($('.product-list-filter__price-slider .jquery-slider').size()) {
        
        var $price_slider = $('.product-list-filter__price-slider');
        
        $('.product-list-filter__price-slider .jquery-slider').slider({
    		range: true,
    		min: 0,
    		max: 5000,
    		values: [ 0, 3800 ],
    		slide: function( event, ui ) {
    			
                $price_slider.find( ".product-list-filter__price-slider__value-from" ).html( ui.values[ 0 ] + " Kč" );
    			$price_slider.find( ".product-list-filter__price-slider__value-to" ).html( ui.values[ 1 ] + " Kč" );
    			
    		}
    	});
        
    }
    //Product list filter - price slider
    
    
    
    //Show search form
    $('.js-show-search-form').click(function(e) {
        
        e.preventDefault();
        
        $('#header-search').toggle();
        $('.search-form input[type=text]').focus();
        
    });
    //Show search form
    
    
    
    //Close mobile navigation
	$('.js-close-mobile-navigation').click(function(e) {
    	
        e.preventDefault();
        
        $('body').removeClass('pushy-open-left');	
    	
    	
	});
	//Close mobile navigation
	
	
	
	//Swipebox
	if($('.img-gallery').size()) {
    	
        $('.img-gallery').swipebox({
            hideBarsDelay: false
        });
        
    }
    //Swipebox
    
    
    
    //Handle autocomplete
    $('.js-handle-autocomplete').keyup(function(e) {
    
        var $min_chars_to_search = 3;
        var $num_chars = $(this).val().length;
        var $key_code = e.keyCode || e.which;
        
        if($key_code == 27) {
            
            $('.autocomplete').hide();
            
        } else if($num_chars >= $min_chars_to_search) {
            
            //Search code here => if($results) {show autocomplete} else {hide autocomplete}
            
            $('.autocomplete').show();
            
        } else {
            
            $('.autocomplete').hide();
            
        }
        
    });
    
    $(document).mouseup(function(e) {
	    
	    var container = $('.autocomplete');
	
	    // if the target of the click isn't the container nor a descendant of the container
	    if (!container.is(e.target) && container.has(e.target).length === 0) 
	    {
	        container.hide();
	    }
	    
	});
    //Handle autocomplete
    
    
    
    //Datepicker
	if($('.js-datepicker').size()) {
    	
    	$.datepicker.regional['cs'] = {
            closeText: 'Zavřít',
            prevText: 'Předchozí',
            nextText: 'Další',
            currentText: 'Hoy',
            monthNames: ['Leden','Únor','Březen','Duben','Květen','Červen', 'Červenec','Srpen','Září','Říjen','Listopad','Prosinec'],
            monthNamesShort: ['Le','Ún','Bř','Du','Kv','Čn', 'Čc','Sr','Zá','Ří','Li','Pr'],
            dayNames: ['Neděle','Pondělí','Úterý','Středa','Čtvrtek','Pátek','Sobota'],
            dayNamesShort: ['Ne','Po','Út','St','Čt','Pá','So',],
            dayNamesMin: ['Ne','Po','Út','St','Čt','Pá','So'],
            weekHeader: 'Sm',
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
                
        $.datepicker.setDefaults($.datepicker.regional[ "cs" ]);
    	
        $('.js-datepicker').datepicker({
	        //beforeShowDay: $.datepicker.noWeekends //vypnute vikendy
	        //minDate: new Date(2020, 2, 1) //minimalni datum, ktere dovolim vybrat
	    });
        
    }
    //Datepicker
    
    
    
    //Close notification
    $('.js-close-notification').click(function(e) {
        
        e.preventDefault();
        
        $(this).parent().fadeOut(); 
        
    });
    
});